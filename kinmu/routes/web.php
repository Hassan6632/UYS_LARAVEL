<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('product', function () {
    return view('product');
});
Route::get('catagory', function () {
    return view('catagory');
});
Route::get('checkout', function () {
    return view('checkout');
});
Route::get('cart', function () {
    return view('cart');
});
Route::get('login', function () {
    return view('login');
});
Route::get('register', function () {
    return view('register');
});

Route::post('productCreate', 'ThemeController@store')->name('createProduct');


/* ->->->->->->->->->->->->->->->->->
    Admin Panel Route
 <-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<- */

Route::get('button', function () {
    return view('admin.buttons');
});
Route::get('calendar', function () {
    return view('admin.calendar');
});
Route::get('charts', function () {
    return view('admin.charts');
});
Route::get('chat', function () {
    return view('admin.chat');
});
Route::get('403-error', function () {
    return view('admin.error403');
});
Route::get('404-error', function () {
    return view('admin.error404');
});
Route::get('405-error', function () {
    return view('admin.error405');
});
Route::get('500-error', function () {
    return view('admin.error500');
});
Route::get('form-common', function () {
    return view('admin.form-common');
});
Route::get('form-validation', function () {
    return view('admin.form-validation');
});
Route::get('form-wizard', function () {
    return view('admin.form-wizard');
});
Route::get('gallery', function () {
    return view('admin.gallery');
});
Route::get('grid', function () {
    return view('admin.grid');
});
Route::get('admin', function () {
    return view('admin.index');
});
Route::get('index2', function () {
    return view('admin.index2');
});
Route::get('interface', function () {
    return view('admin.interface');
});
Route::get('invoice', function () {
    return view('admin.invoice');
});
Route::get('login', function () {
    return view('admin.login');
});
Route::get('tables', function () {
    return view('admin.tables');
});
Route::get('widget', function () {
    return view('admin.widgets');
});
/* ->->->->->->->->->->->->->->->->->
    End Admin Panel Route
 <-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<- */