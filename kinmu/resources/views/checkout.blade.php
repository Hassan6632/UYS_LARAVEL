@include('assets.header')
<div class="check">
			<div class="container"> 
				<div class="check-top text-center"> 
					<h3>Checkout</h3>
					<span>Returning customer? <a href="#"> Click here to login</a></span>
				</div>
				<div class="row">
					<div class="col-md-8">
						<form> 
							<div class="form-group">
								<label for="country">Country</label>
								<datalist id="country"> 
									<option>bangladesh</option>
									<option>india</option>
									<option>pakistan</option>
									<option>nepal</option>
									<option>votan</option>
									<option>srilanks</option>
									<option>mayanmar</option>
								</datalist>
								<input type="text" list="country" class="form-control" />
						  </div>
							<div class="row">
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="fname">first name</label>
									<input type="text" class="form-control" id="fname" placeholder="first name" autofocus required>
								  </div>
								</div>
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="fname">lst-name</label>
									<input type="text" class="form-control" id="lst-name" placeholder="last name" autofocus required>
								  </div>
								</div>
							</div>
							<div class="form-group">
								<label for="company">company name</label>
								<input type="text" class="form-control" id="company" placeholder="company name" autofocus required>
							</div>
							<div class="form-group">
								<label for="address">address</label>
								<textarea type="text" class="form-control" id="address" placeholder="address"></textarea>
							</div>
							<div class="row">
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="district">district</label>
									<input type="text" class="form-control" id="district" placeholder="district name" autofocus required>
								  </div>
								</div>
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="postcode">postcode/zip*</label>
									<input type="text" class="form-control" id="postcode" placeholder="postcode" autofocus required>
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="email">email</label>
									<input type="email" class="form-control" id="email" placeholder="district name"required>
								  </div>
								</div>
								<div class="col-md-6"> 
									<div class="form-group">
									<label for="phone">phone number</label>
									<input type="number" name="quantity" min="7" max="13" class="form-control" id="phone number" placeholder="phone number"required>
								  </div>
								</div>
							</div>
							<div class="form-group different-address">
								<input type="checkbox" id="differ" name="vehicle1" value="Bike">
								<label for="differ">Ship to a different address?</label>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<div class="purchase"> 
							<h3>Your purchase</h3>
							<div class="check-price">
								<div class="row"> 
									<div class="col-md-6"> 
										<div class="pur-head text-left"> 
											<h5>product</h5>
										</div>
									</div>
									<div class="col-md-6">
										<div class="pur-head text-left"> 
											<h5>total</h5>
										</div>
									</div>
								</div>
							</div>
							<div class=" check-price"> 
								<div class="row"> 
									<div class="col-md-6"> 
										<div class="check-p-name">
											<h5>Dress</h5>
											<h5><span>color :</span> blue</h5>
										</div>
									</div>
									<div class="col-md-6">
										<div class="check-rate"> <br />
											<h5>tk 2500.00</h5>
										</div>
									</div>
								</div>
							</div>
							<div class=" check-price"> 
								<div class="row"> 
									<div class="col-md-6"> 
										<div class="check-p-name">
											<h5>subtotal</h5>
											<h5>shipping</h5>
										</div>
									</div>
									<div class="col-md-6">
										<div class="check-rate">
											<h5>tk 2500.00</h5>
											<h5>free</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="card"> 
								<div class="form-group card-txt">
									<input type="radio" id="cheque" name="vehicle1" value="Bike">
									<label for="cheque">Cheque Payment</label>
								</div>
								<div class="form-group card-txt">
									<input type="radio" id="cash" name="vehicle1" value="Bike">
									<label for="cash">Cash on Delivery </label>
								</div>
								<div class="form-group card-txt">
									<input type="radio" id="paypal" name="vehicle1" value="Bike">
									<label for="paypal">PayPal <img class="img-responsive" src="assets/img/checkout.png" alt="checkout" /></label>
								</div>
								<div class="cart-btn text-center"> 
									<a href="#">place order</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


@include('assets.footer')
@include('assets.modal')
@include('assets.btmjs')