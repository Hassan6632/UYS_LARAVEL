@include('assets.header')
        <div class="cart"> 
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-912 col-xs-12"> 
						<div class="cate-hd-txt">
							<div class="row">
								<div class="col-md-6"> 
									<div class="cate-head text-left"> 
										<h2>shopping cart</h2>
									</div>
								</div>
								<div class="col-md-6"> 
									<div class="sort text-right"> 
										<a href="#"><span>sort by:</span> popularity <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
						</div>
						<div class="row"> 
							<div class="cart-list text-center"> 
								<div class="row">
									<div class="col-md-4">
										<h4>product</h4>
									</div>
									<div class="col-md-2"> 
										<h4>price</h4>
									</div>
									<div class="col-md-2">
										<h4>quantity</h4>
									</div>
									<div class="col-md-2"> 
										<h4>total</h4>
									</div>
									<div class="col-md-2"> 
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="cart-pic"> 
											<img class="img-responsive" src="assets/img/woman/gp2.jpg" alt="MAN's Dress" />
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-price">
											<h5>69</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="cart-quantity"> 
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <i onclick="minus()" class="fa fa-minus" aria-hidden="true">-</i>
                                                </div>
											</div>
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <h4 id="result">1</h4>
                                                </div>
											</div>
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <i onclick="plus()" class="fa fa-plus" aria-hidden="true"></i>
                                                </div>
											</div>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-total"> 
											<h5>2500</h5>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-cancle"> 
											<i class="fa fa-times" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="cart-pic"> 
											<img class="img-responsive" src="assets/img/woman/gp3.jpg" alt="MAN's Dress" />
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-price">
											<h5>69</h5>
										</div>
									</div>
									<div class="col-md-2">
										<div class="cart-quantity"> 
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <i onclick="minus()" class="fa fa-minus" aria-hidden="true">-</i>
                                                </div>
											</div>
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <h4 id="result">1</h4>
                                                </div>
											</div>
											<div class="cart-plus"> 
												<div class="pro-quan"> 
                                                    <i onclick="plus()" class="fa fa-plus" aria-hidden="true"></i>
                                                </div>
											</div>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-total"> 
											<h5>2500</h5>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="cart-cancle"> 
											<i class="fa fa-times" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="total-cart-amount text-center"> 
									<div class="row">
										<div class="col-md-offset-7 col-md-3">
											<h5>cart total</h5>
										</div>
										<div class="col-md-2">
											<h5>5000</h5>
										</div>
									</div>
								</div>
								<div class="mod-btn cart-btn"> 
									<a href="#">update cart</a>
									<a href="#">proceed to checkout</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@include('assets.footer')
@include('assets.modal')
@include('assets.btmjs')