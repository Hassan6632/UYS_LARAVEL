<!DOCTYPE HTML>
<html lang="en-US">
	<head>
    
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>KinMu</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Google Font Link-->
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,700,800,800italic,900' rel='stylesheet' type='text/css'>
			
			
			
			
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/font-awesome.min.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/animate.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/owl.carousel.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/owl.theme.default.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap.min.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap-social.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/yamm.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/mega-menu.css')}}" media="all" /> 
		
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/round.css')}}" media="all" /> 
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/style.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/responsive.css')}}" media="all" /> 
		
		 <!-- Place favicon.ico in the root directory -->
		<link rel="shortcut icon" href="assets/img/favicon.png" >
		
		<script src="{{URL::asset('https://code.jquery.com/jquery-3.1.1.min.js')}}"></script>
		<script src="{{URL::asset('assets/js/jquery.3.1.1.min.js')}}"></script>
		<script src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
	</head>
	<body>
		<header id="main-menu" class="navbar-fixed-top ">
			<div class="container">
				<div class="menu "> 
					<nav class="navbar yamm navbar-default " role="navigation">
						<a class="navbar-brand kinmu-logo" href="{{URL('home')}}">
							<div class="logo"> 
								<img src="assets/img/logo4.png" alt="LOGO" />
							</div>
						</a>
						 <ul class="nav navbar-nav menu-gap-top">
						   <li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">MANZ</a>
								 <ul class="dropdown-menu">
								   <li>
									   <div class="yamm-content">
										  <div class="row"> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-3"> 
												<div class="menutitle text-center"> 
													
													<h4>MANZ ZONE</h4>
													<img class="img-responsive" src="assets/img/man/01.jpg" alt="MANZ" />
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-1"> 
												<div class="man-right-add"> 
													<img src="assets/img/sale02.jpg" alt="offer" />
												</div>
											</div>
										  </div>
									   </div>
								   </li>
								 </ul>
						   </li>
						 </ul>
						 <ul class="nav navbar-nav menu-gap-top">
						   <li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">GIRLZ</a>
								 <ul class="dropdown-menu">
								   <li>
									   <div class="yamm-content">
										  <div class="row"> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-3"> 
												<div class="menutitle text-center"> 
													<h4>girl ZONE</h4>
													<img class="img-responsive" src="assets/img/woman/GP2.jpg" alt="MANZ" />
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-1"> 
												<div class="man-right-add"> 
													<img src="assets/img/sale02.jpg" alt="offer" />
												</div>
											</div>
										  </div>
									   </div>
								   </li>
								 </ul>
						   </li>
						 </ul>
						 <ul class="nav navbar-nav menu-gap-top">
						   <li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">KIDZ</a>
								 <ul class="dropdown-menu">
								   <li>
									   <div class="yamm-content">
										  <div class="row"> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div> 
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-3"> 
												<div class="menutitle text-center"> 
													<h4>KID ZONE</h4>
													<img class="img-responsive" src="assets/img/kid/01.jpg" alt="MANZ" />
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-2"> 
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
												<div class="man-menu-cetagory"> 
													<h4>Cetagory</h4>
													<ul>
														<li><a href="#">man cloth</a></li>
														<li><a href="#">man Pant</a></li>
														<li><a href="#">Belt</a></li>
														<li><a href="#">Sunglass</a></li>
														<li><a href="#">Watches</a></li>
													</ul>
												</div>
											</div>
											<div class="col-md-1"> 
												<div class="man-right-add"> 
													<img src="assets/img/sale02.jpg" alt="offer" />
												</div>
											</div>
										  </div>
									   </div>
								   </li>
								 </ul>
						   </li>
						 </ul>
						 
						 <ul class="nav navbar-nav menu-gap-top">
						   <li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">HOME ACCESSORIES </a>
							 <ul class="dropdown-menu">
							   <li>
								   <div class="yamm-content">
									  <div class="row"> 
										<ul class="list-group">
											<li class="list-group-item"><a href="#">Badroom Furniture</a></li>
											<li class="list-group-item"><a href="#">Kitchen Furniture</a></li>
											<li class="list-group-item"><a href="#">Drowing Furniture</a></li>
											<li class="list-group-item"><a href="#">Other Furniture</a></li>
										</ul>
									  </div>
									</div>
							   </li>
							 </ul>
						   </li>
						 </ul>
						 <ul class="nav navbar-nav menu-gap-top">
						   <li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">ELECTRONICS</a>
							 <ul class="dropdown-menu">
							   <li>
								   <div class="yamm-content">
									  <div class="row"> 
										<ul class="list-group">
											<li class="list-group-item"><a href="#">Badroom Furniture</a></li>
											<li class="list-group-item"><a href="#">Kitchen Furniture</a></li>
											<li class="list-group-item"><a href="#">Drowing Furniture</a></li>
											<li class="list-group-item"><a href="#">Other Furniture</a></li>
										</ul>
									  </div>
									</div>
							   </li>
							 </ul>
						   </li>
						 </ul>
						  <ul class="nav navbar-nav navbar-right menu-gap-top ">
							 <li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<div class="cart-bar text-center"> 
									<p class="cart-number">15</p>
									<img src="assets/img/cart/cart.png" alt="CART" />
									<p>add to cart</p>
								</div>
							 </a>
							 <ul class="dropdown-menu">
							   <li>
								   <div class="yamm-content">
										
											<div class="cart-list-menu"> 
												<a href="#">
												<div class="cart-list-pic"> 
													<img src="assets/img/man/03.jpg" alt="" />
												</div>
												<div class="list-detail-cart"> 
													<h5>Product name</h5>
													<h6><span>RATE: </span>69$</h6>
												</div>
												</a>
											</div>
											<div class="cart-list-menu"> 
												<a href="#">
												<div class="cart-list-pic"> 
													<img src="assets/img/man/03.jpg" alt="" />
												</div>
												<div class="list-detail-cart"> 
													<h5>Product name</h5>
													<h6><span>RATE: </span>69$</h6>
												</div>
												</a>
											</div>
											<div class="cart-list-menu text-center"> 
											    <hr>
												<a href="{{URL('/checkout')}}">
												<span class="cart-btn">checkout</span>
												</a>
											</div>
										
									</div>
							   </li>
							 </ul>
						   </li>
						 </ul>
						  <ul class="nav navbar-nav navbar-right menu-gap-top ">
							 <li class="dropdown">
							 <a action="_blank" href="{{URL('/login')}}">
								<div class="cart-bar sign-in text-center"> 
									<img src="assets/img/cart/boy.png" alt="SIGN IN" />
									<p>Sign In</p>
								</div>
							 </a>
						   </li>
						 </ul>
						 <ul class="nav navbar-nav navbar-right menu-gap-top ">
							<li class="search">
							 <a href="#" class="search-toggle" data-toggle="dropdown">
								<div class="search-bar text-center"> 
									<input type="text" placeholder="Search"/>
									<i class="fa fa-search" aria-hidden="true"></i>
								</div>
							 </a>
							</li>
						  </ul>
					</nav>
				</div>
			</div>
		</header>