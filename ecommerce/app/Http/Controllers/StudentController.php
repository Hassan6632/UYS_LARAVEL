<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\category;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



   

    public function index()
    {
        $products = product::all();
        return view('admin.product-all', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //select * from $table where id = 1 || $s = Student::find(1) || $s->delete()
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request['title'];
        $description = $request['description'];
        $price = $request['price'];
        $image = $request['im'];
        $cat = $request['cat'];

        $product = new product;
        $product->title = $title;
        $product->description = $description;
        $product->price = $price;
        $product->image = $image;
        $product->cat = $cat;

        if($product->save())
            return view('admin.productCreate', compact('title', 'description', 'price', 'image'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = product::findOrFail($id);
        return view('admin.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = product::find($id);
        return view('admin.product-edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $category = category::find($id);
        // $category->delete();
    }

    public function check()
    {
        $a = product::all();
        
        /*return view('contact', compact('x', 'y','a', 'b'));*/ // withId($id) 

        return view('contact')->withAn($a);
        //return $id;
    }

     public function allProduct()
    {
        $product = product::all();
        return view('admin.allproducts', compact('product'));
    }

    public function man()
    {
        $product = product::all();
        return view('home', compact('product'));
    }

    public function cat(Request $request)
    {
        $name = $request['category'];
        $cat = new category;
        $cat->name = $name;

        if($cat->save())
            return view('admin.show-category', compact('name'));
    }

    public function showCat(){
       
        $allcat = category::paginate(3);
        return view('admin.category', compact('allcat'));
    }

     public function showCatForCreate(){
       
        $allcat = category::all();
        return view('admin.create', compact('allcat'));
    }
}
