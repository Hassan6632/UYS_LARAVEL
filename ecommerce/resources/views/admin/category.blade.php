@extends('admin.adminLayout')

@section('content')
<form action="{{route('category')}}" method="POST">
          <div>
            
            <input type="text"  id="title" name="category" placeholder="name">
            {{csrf_field()}}
          </div>

          <button type="submit" class="btn btn-default">Add Category</button>
      </form>

      <table>
      @foreach($allcat as $cat)
        <tr>
          <td>{{$cat->name}}</td>
        </tr> 
      @endforeach 
      </table>

      <span>{{$allcat->links()}}</span>

  @stop