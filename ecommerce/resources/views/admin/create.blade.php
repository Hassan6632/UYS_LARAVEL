@extends('admin.adminLayout')

@section('content')
<!-- Large modal -->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <iframe  width="765" height="550" frameborder="0"
        src="/filemanager/dialog.php?type=1&field_id=iminput">
      </iframe>
    </div>
  </div>
</div>


  <div style="margin:auto; width: 80%;">
    <div class="container">
      <form action="{{route('createProduct')}}" method="POST">
          <div>
            <label for="title">Product Title</label>
            <input type="text"  id="title" name="title" placeholder="Title">
            {{csrf_field()}}
          </div>
          <select name="cat">
              @foreach($allcat as $cat)
            <option value="{{$cat->id}}">{{$cat->name}}</option>
            @endforeach
          </select>
          <label for="Description">Product Description</label>
          <textarea name="description"></textarea>
          {{csrf_field()}}
          <div>
            <label for="Price">Product Price</label>
            <input type="text"  id="price" name="price" placeholder="Price">
            {{csrf_field()}}
          </div>
          <div>
            <input type="text" id="iminput" name="im">
          </div>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
            <a href="">Featured Image</a>
          </button>

          <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div>
    <script type="text/javascript" src="{{URL::to('tinymce/tinymce.min.js')}}"></script>

    <script type="text/javascript">
      tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help filemanager'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
        image_advtab: true,
        templates: [
          { title: 'Test template 1', content: 'Test 1' },
          { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ],
        external_filemanager_path:"/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/filemanager/plugin.min.js"},
        relative_urls: false
      });
    </script>
  </div>
@stop