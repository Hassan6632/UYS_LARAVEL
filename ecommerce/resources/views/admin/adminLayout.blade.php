@include('admin.adminass.admincss')
@include('admin.adminass.menu')


<!--main-container-part-->
<div id="content">
  @yield('content')
</div>
<!--end-main-container-part-->

<!--Footer-part-->
@include('admin.adminass.footer')