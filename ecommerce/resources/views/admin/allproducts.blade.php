@extends('admin.adminLayout')
@section('content')
<div class="table-responsive">
  <table class="table">
  		<tr>
  			<th>Serial</th>
  			<th>Title</th>
  		</tr>
  	@foreach($product as $pro)
    	<tr class="success">
		  <td class="active">{{$pro->id}}</td>
		  <td class="active"><a href="product/{{$pro->id}}">{{$pro->title}}</td>
		</tr>
	@endforeach
  </table>
</div>

@stop