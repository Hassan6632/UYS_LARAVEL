<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>eCommerce</title>
		<meta name="description" content="">
		<!-- {{URL::asset('assets/js/jquery.min.js')}} -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Google Font Link-->
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,700,800,800italic,900' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/round.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="assets/css/animate.css" media="all" />
		<link rel="stylesheet" type="text/css" href="assets/css/round.css" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/owl.carousel.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/owl.theme.default.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap.min.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap-social.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/responsive.css')}}" media="all" />
		
		 <!-- Place favicon.ico in the root directory -->
		<link rel="shortcut icon" href="assets/img/favicon.png" >
		

		<script src="{{URL::asset('assets/js/jquery.3.1.1.min.js')}}"></script>
		<script src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
	</head>
	<body>
		 <header> 
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="logo"> 
								<img class="img-responsive" src="img/logo2.png" alt="KINMU.COM" />
							</div>
						</div>
						<div class="col-md-9 col-sm-9 col-xs-12"> 
							<div class="top-menu text-right">
								<ul>       
									<li><a href="#">Behind Punoh</a></li>
									<li><a href="#">Social Enterprise</a></li>
									<li><a href="#">How it works</a></li>
									<li><a href="#">Projects</a></li>
									<li><a href="#"> Contact </a></li>
									<li><a href="#">Register</a></li>
									<li><a href="#">Sign in</a></li>
								</ul>
							</div>
							<div class="row">
								<div class="col-md-9  col-sm-9 col-xs-12"> 
									<div class="search"> 
										<form action="search"> 
											<input type="text"  placeholder="Search" />
											<button>search</button>
										</form>
									</div>
								</div>
								<div class="col-md-3  col-sm-3 col-xs-12">
									<div class="addto-cart text-center">
										<div class="cart"> 
											<img src="img/cart/cart.png" alt="CART" /><br />
											<span>add to cart</span>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</header>