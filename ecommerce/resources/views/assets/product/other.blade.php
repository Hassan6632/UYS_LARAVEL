<!-- others -->
		<div class="product">
			<div class="container">
				<div class="pro-heading"> 
					<img class="img-responsive" src="img/cart/gift.png" alt="MAN"/>
					<h3>other</h3>
				</div>
				<div class="row"> 
					<div class="product-slide owl-carousel owl-theme">
						<div class="item">
							<div class="single-product"> 
								<img src="img/woman/gp1.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="img/woman/gp2.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="img/woman/gp3.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="img/woman/gp6.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- others -->