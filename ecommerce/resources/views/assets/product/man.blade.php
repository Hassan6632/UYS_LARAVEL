<!-- man zone -->
		<div class="product">
			<div class="container">
				<div class="pro-heading"> 
					<img class="img-responsive" src="img/cart/boy.png" alt="MAN"/>
					<h3>man zone</h3>
				</div>
				<div class="row">
				@foreach($product as $pro)
					<div class="col-md-3 col-sm-6 col-xs-12">

							<div class="single-product"> 
								<img src="{{$pro->image}}" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>{{$pro->price}}</h3>
									<p>{{$pro->title}}</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						
					</div>
					@endforeach
				</div>
				
			</div>
		</div><!-- End man zone -->