	$('.single-slide').owlCarousel({

		loop:true,
		items:1,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:true,
		singleItem:true,
		
});
	$('.product-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:4,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:false,
		singleItem:true,
		responsive:{
        0:{
            items:1,
            nav:true
        },
        500:{
            items:2,
            nav:false
        },
		750:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:false,
            loop:true
        }
    }
		
});
$('.categiry-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:3,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:false,
		singleItem:true,
		responsive:{
        0:{
            items:1,
            nav:true
        },
        500:{
            items:2,
            nav:false
        },
		750:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
		
});

$('.modal-big-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:1,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:3000,
		dots:false,
		singleItem:true,
		
		
});
$('.modal-slamm-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:4,
		margin:0,
		nav:true,
		autoplay:true,
		autoplayTimeout:3000,
		dots:false,
		singleItem:true,
		
		
});

/* Round Slide */
$(function() {
				$('#manz').carouFredSel({
					direction: 'up',
					items: 1,
					scroll: {
						fx: 'directscroll'
					},
					pagination: {
						container: '#manz-pager',
						anchorBuilder: function( nr ) {
							return '<a href="#" class="thumb' + nr + '"><img src="' + this.src + '" width="100" /></a>';
						}
					}
				});
			});
			$(function() {
				$('#girls').carouFredSel({
					direction: 'up',
					items: 1,
					scroll: {
						fx: 'directscroll'
					},
					pagination: {
						container: '#girls-pager',
						anchorBuilder: function( nr ) {
							return '<a href="#" class="thumb' + nr + '"><img src="' + this.src + '" width="100" /></a>';
						}
					}
				});
			});
/* Modal */
$('#cart').on('shown.bs.modal', function () {
			  $('#myInput').focus()
			})