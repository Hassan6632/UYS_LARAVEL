<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('about', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('home');
});

Route::get('product', function () {
    return view('product');
});
Route::get('bank', function () {
    return view('layout.abc');
});

Route::get('hello', function () {
    return "Hello Laravel";
});
Route::get('adminhome', function () {
    return view('admin.adminLayout');
});

Route::get('dashboard', function () {
    return view('admin.dashboard');
});
Route::get('adminhome', function () {
    return view('admin.buttons');
});

Route::get('adminhome', function () {
    return view('admin.calendar');
});

Route::get('create', function () {
    return view('admin.create');
});

Route::post('productCreate', 'StudentController@store')->name('createProduct');

Route::get('product/{id}', 'StudentController@show');

Route::get('adminhome', function () {
    return view('admin.chat');
});

Route::get('adminhome', function () {
    return view('admin.error404');
});

Route::get('adminhome', function () {
    return view('admin.from-validation');
});

Route::get('adminhome', function () {
    return view('admin.form-wizard');
});

Route::get('adminhome', function () {
    return view('admin.gallery');
});

Route::get('adminhome', function () {
    return view('admin.form-common');
});

Route::get('adminhome', function () {
    return view('admin.grid');
});

Route::get('adminhome', function () {
    return view('admin.index2');
});

Route::get('adminhome', function () {
    return view('admin.interface');
});

Route::get('adminhome', function () {
    return view('admin.login');
});

Route::get('adminhome', function () {
    return view('admin.tables');
});

Route::get('adminhome', function () {
    return view('admin.adminhome');
});


Route::resource('check', 'StudentController');

Route::get('contact', 'StudentController@check');
Route::get('product-edit/{id}/edit', 'StudentController@edit');
Route::get('product-all', 'StudentController@index');

Route::get('allproduct', ['middleware'=>'auth'], 'StudentController@allProduct');
//Route::get('/', 'StudentController@man');

Route::post('show-category', 'StudentController@cat')->name('category');

Route::get('category', 'StudentController@showCat');
Route::get('create', 'StudentController@showCatForCreate');

// Route::delete('allcat/{id}', 'StudentController@destroy');
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
