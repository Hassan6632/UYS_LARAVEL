	$('.single-slide').owlCarousel({

		loop:true,
		items:1,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:true,
		singleItem:true,
		
});
	$('.product-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:4,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:false,
		singleItem:true,
		responsive:{
        0:{
            items:1,
            nav:true
        },
        500:{
            items:2,
            nav:false
        },
		750:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:false,
            loop:true
        }
    }
		
});
$('.categiry-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:3,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:4000,
		dots:false,
		singleItem:true,
		responsive:{
        0:{
            items:1,
            nav:true
        },
        500:{
            items:2,
            nav:false
        },
		750:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:false,
            loop:true
        }
    }
		
});

$('.modal-big-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:1,
		margin:0,
		nav:false,
		autoplay:true,
		autoplayTimeout:3000,
		dots:false,
		singleItem:true,
		
		
});
$('.modal-slamm-slide').owlCarousel({

		loop:true,
		margin:10,
		responsiveClass:true,
		items:4,
		margin:0,
		nav:true,
		autoplay:true,
		autoplayTimeout:3000,
		dots:false,
		singleItem:true,
		
		
});

/* Round Slide */
$(function() {
				$('#manz').carouFredSel({
					direction: 'up',
					items: 1,
					scroll: {
						fx: 'directscroll'
					},
					pagination: {
						container: '#manz-pager',
						anchorBuilder: function( nr ) {
							return '<a href="#" class="thumb' + nr + '"><img src="' + this.src + '" width="80" /></a>';
						}
					}
				});
			});
			$(function() {
				$('#girls').carouFredSel({
					direction: 'up',
					items: 1,
					scroll: {
						fx: 'directscroll'
					},
					pagination: {
						container: '#girls-pager',
						anchorBuilder: function( nr ) {
							return '<a href="#" class="thumb' + nr + '"><img src="' + this.src + '" width="80" /></a>';
						}
					}
				});
			});
/* Modal */
		$('#cart').on('shown.bs.modal', function () {
			  $('#myInput').focus()
			})
			
/*mega menu*/

		$(document).on('click', '.yamm .dropdown-menu', function(e) {
			  e.stopPropagation()
			})
			
			
			jQuery('ul.nav li.dropdown').hover(function() {
			jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
			}, function() {
			jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
			});
		$(document).ready(function(){
			$('.fa-search').click(function(){
				$('input').toggle('slow');
			});
		});
		
		/*cart btn*/
		function minus(){
				var min = document.getElementById('result').innerHTML;
				
				if(min>0){
					min--;
					document.getElementById('result').innerHTML=min;
				}
			}
			function plus(){
				var pl = Number(document.getElementById('result').innerHTML);
				
				if(pl<10){
					pl++;
					document.getElementById('result').innerHTML=pl;
				}
			}

