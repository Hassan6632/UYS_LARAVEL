<!-- Home Accessories -->
		<div class="product">
			<div class="container">
				<div class="pro-heading"> 
					<img class="img-responsive" src="assets/img/cart/home.png" alt="MAN"/>
					<h3>Home Accessories</h3>
				</div>
				<div class="row"> 
					<div class="product-slide owl-carousel owl-theme">
						<div class="item">
							<div class="single-product"> 
								<img src="assets/img/home/01.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="assets/img/home/02.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="assets/img/home/03.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="assets/img/home/04.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="single-product"> 
								<img src="assets/img/home/05.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Home Accessories -->