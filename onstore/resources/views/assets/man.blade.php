<!-- man zone -->
		<div class="product">
			<div class="container">
				<div class="pro-heading"> 
					<img class="img-responsive" src="assets/img/cart/boy.png" alt="MAN"/>
					<h3>man zone</h3>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single-product"> 
							<img src="assets/img/man/bp4.jpg" alt="MAN's Dress" />
							<div class="pro-price text-center"> 
								<h3>$69</h3>
								<p>Easy Lorem Ipsum Edition</p>
								<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
							</div>
						</div>
						<div class="single-product"> 
							<img src="assets/img/man/bp1.jpg" alt="MAN's Dress" />
							<div class="pro-price text-center"> 
								<h3>$69</h3>
								<p>Easy Lorem Ipsum Edition</p>
								<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12"> 
						<div id="border"></div>
							<div id="wrapper">
								<div id="manz">
									<img class="img-responsive round-slide" src="assets/img/man/01.jpg"/>
									<img class="img-responsive round-slide" src="assets/img/man/03.jpg"/>
									<img class="img-responsive round-slide" src="assets/img/man/04.jpg"/>
									<img class="img-responsive round-slide" src="assets/img/man/bp4.jpg"/>
									<img class="img-responsive round-slide" src="assets/img/man/bp3.jpg"/>
								</div>
							</div>
						<div id="manz-pager"></div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single-product"> 
							<img src="assets/img/man/bp.jpg" alt="MAN's Dress" />
							<div class="pro-price text-center"> 
								<h3>$69</h3>
								<p>Easy Lorem Ipsum Edition</p>
								<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
							</div>
						</div>
						<div class="single-product"> 
							<img src="assets/img/man/bp3.jpg" alt="MAN's Dress" />
							<div class="pro-price text-center"> 
								<h3>$69</h3>
								<p>Easy Lorem Ipsum Edition</p>
								<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div><!-- End man zone -->