		<footer> 
			<div class="container"> 
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12"> 
						<div class="foter-menu"> 
							<h3>My account</h3>
							<ul>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>My orders</a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>My credit slips</a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>My addresses</a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>My personal info</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-offset-2 col-md-3 col-sm-4 col-xs-12"> 
						<div class="foter-menu"> 
							<h3>information</h3>
							<ul>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>New products </a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Best sellers </a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Contact us</a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Terms and conditions of use </a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>about us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-3 col-sm-4 col-xs-12"> 
						<div class="foter-menu"> 
							<h3>Why buy from us</h3>
							<ul>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Orders & Returns </a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Advance Search</a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Affiliates </a></li>
								<li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i>Group Sales </a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="copy-r text-center"><p> &copy; 2017 Punoh. All rights reserved. Designed and Developed by:Web Tricks Ltd.</p></div>