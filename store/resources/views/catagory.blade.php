@include('assets.header')
<div class="filter"> 
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div class="filtering"> 
							<div class="filt-top text-center"> 
								<h4>FILTER PRODUCTS BY</h4>
							</div>
							<div class="range">
								<h5>Price range</h5>
							  <form>
								<div class="range3">
								
								<output for="range2" class="output3 text-right"></output>
								<input type="range" name="range2" min="0" max="1000000" step="80" value="1500">
								
								</div>
							  </form>
							</div>
							<div class="size"> 
								<h5>by size</h5>
								<form action="size">
									<datalist id="sname"> 
											<option>xxs</option>
											<option>xs</option>
											<option>xxl</option>
											<option>xl</option>
											<option>s</option>
											<option>m</option>
											<option>l</option>
											<option>4xl</option>
											<option>5xl</option>
											<option>6xl</option>
										</datalist>
										<input type="text" list="sname" class="form-control" placeholder="sx, xl, s, m ..." />
								</form>
							</div>
							<div class="size"> 
								<h5>by color</h5>
								<form action="size">
									<datalist id="color"> 
											<option>Blue</option>
											<option>Red</option>
											<option>Black</option>
											<option>White</option>
											<option>Pink</option>
											<option>Green</option>
										</datalist>
										<input type="text" list="color" class="form-control" placeholder="Red, Green ..." />
								</form>
							</div>
							<div class="size"> 
								<h5>Brand</h5>
								<form action="brand">
									<datalist id="brand"> 
											<option>Oxolloxo</option>
											<option>Hypnotex </option>
											<option>Diva Fashion</option>
											<option>Rain & Rainbow</option>
											<option>Smartex</option>
											<option>Yellow</option>
										</datalist>
										<input type="text" list="brand" class="form-control" placeholder="Oxolloxo,  Smartex ..." />
								</form>
							</div>
							
						</div>
					</div>
					<div class="col-md-9 col-sm-9 col-xs-12"> 
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12"> 
								<div class="single-product"> 
								<img src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
								<div class="pro-price text-center"> 
									<h3>$69</h3>
									<p>Easy Lorem Ipsum Edition</p>
									<button type="button" class="btn btn-primary mod-butn" data-toggle="modal" data-target=".bs-example-modal-lg">add to cart</button>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@include('assets.footer')
@include('assets.modal')
@include('assets.btmjs')