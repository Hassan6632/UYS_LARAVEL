@include('assets.header')
        <div class="check">
			<div class="container"> 
				<div class="check-top text-center"> 
					<h3>login</h3>
					<span>Don't have an account? <a href="{{URL('/register')}}">sign up</a></span>
				</div>
				<div class="row">
					<div class="col-md-offset-2 col-md-8">
						<form> 
							<div class="log-pag">
								<div class="row">
									<div class="col-md-offset-4 col-md-4"> 
										<div class="form-group">
										<input type="text" class="form-control" id="fname" placeholder="user or email" autofocus required>
									  </div>
									</div>
									<div class="col-md-offset-4 col-md-4"> 
										<div class="form-group">
										<input type="password" class="form-control" id="password" placeholder="password" autofocus required>
									  </div>
									</div>
									<div class="col-md-offset-4 col-md-4"> 
										<div class="form-group remember">
											<input type="checkbox" id="differ" name="vehicle1" value="Bike">
											<label for="differ">remember me</label>
										</div>
									</div>
									<div class="col-md-offset-4 col-md-4"> 
										<div class="form-group">
											<button>login >></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
						
						
				</div>
			</div>
		</div>
@include('assets.footer')
@include('assets.btmjs')