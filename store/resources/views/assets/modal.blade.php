<!-- Large modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header mod-heading">
        <div class="mod-logo"> 
			<img src="assets/img/mlogo.png" alt="" />
		</div>
        <h4 class="modal-title"><span>K</span>inmu.com</h4>
      </div>
	  <div class="modal-body">
		<div class="row">
			<div class="col-md-6"> 
				<div class="big-modal-slide">
					<div class="modal-big-slide owl-carousel owl-theme">
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp1.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp2.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp3.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp6.jpg" alt="MAN's Dress" />
						</div>
					</div>
				</div>
				<div class="small-modal-slide">
					<div class="modal-slamm-slide owl-carousel owl-theme">
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp1.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp2.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp3.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp4.jpg" alt="MAN's Dress" />
						</div>
						<div class="item">
							<img class="img-responsive" src="assets/img/woman/gp6.jpg" alt="MAN's Dress" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6"> 
				<div class="product-detail"> 
					<div class="pro-name"> 
						<h3>Product : Name</h3>
						<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
					</div>
					<div class="dlc"> 
						<i class="fa fa-gift" aria-hidden="true"></i>
						<h3>Deal Code: 57860</h3>
					</div>
					<div class="review"> 
						<div class="rev-txt text-left">
							<h5>review : </h5>
						</div>
						<div class="rev-ico">
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
							<i class="fa fa-star-o" aria-hidden="true"></i>
						</div>
					</div>
					<div class="reg-price"> 
						<strike><h4>reguler price : 2500 tk</h4></strike>
						<h4>offer price : 1500 tk</h4>
						
						<h4>quantity : </h4>
						<div class="pro-quan"> 
							<i onclick="minus()" class="fa fa-minus" aria-hidden="true"></i>
						</div>
						<div class="pro-quan"> 
							<h4 id="result">1</h4>
						</div>
						<div class="pro-quan"> 
							<i onclick="plus()" class="fa fa-plus" aria-hidden="true"></i>
						</div>
					</div>
					
				</div>
				<div class="cart-social"> 
					<div class="row"> 
						<div class="col-md-3 social-buttons">
								<a class="btn btn-block btn-social btn-facebook" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-facebook']);">
									<span class="fa fa-facebook"></span>Facebook
								</a>
							</div>
							<div class="col-md-3 social-buttons"> 
								<a class="btn btn-block btn-social btn-twitter">
									<span class="fa fa-twitter"></span>Twitter
								</a>
							</div>
							<div class="col-md-3 social-buttons"> 
								<a class="btn btn-block btn-social btn-instagram" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-instagram']);">
									<span class="fa fa-instagram"></span>Instagram
								</a>
							</div>
							<div class="col-md-3 social-buttons"> 
								 <a class="btn btn-block btn-social btn-pinterest" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-pinterest']);">
									<span class="fa fa-pinterest"></span>Pinterest
								</a>
							</div>
							<div class="col-md-3 social-buttons"> 
								 <a class="btn btn-block btn-social btn-google" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-google']);">
									<span class="fa fa-google"></span>Google
								</a>
							</div>
							
							<div class="col-md-3 social-buttons"> 
								<a class="btn btn-block btn-social btn-flickr" onclick="_gaq.push(['_trackEvent', 'btn-social', 'click', 'btn-flickr']);">
									<span class="fa fa-flickr"></span> Sign in with Flickr
								</a>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="specification"> 
			<div>
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#specification" aria-controls="specification" role="tab" data-toggle="tab">specification</a></li>
				<li role="presentation"><a href="#money-back" aria-controls="money-back" role="tab" data-toggle="tab">money back</a></li>
			  </ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="specification">
					<h3>Product : Name</h3>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
				</div>
				<div role="tabpanel" class="tab-pane" id="money-back">
					<h3>Money Back Plicy</h3>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.It is a long established fact that a.</p>
				</div>
			  </div>

			</div>
		</div>
      </div>
	  <div class="modal-footer">
		<div class="mod-btn"> 
			<a target="_blank" href="cart.html">add to cart</a>
			<a href="#">add to whitelist</a>
		</div>
      </div>
    </div>
  </div>
</div>